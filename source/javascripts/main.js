jQuery(document).ready(function(){
    jQuery('.owl-carousel').owlCarousel({
        center: true,
        items:1,
        loop:true
    });

jQuery('.more-link').remove();
    jQuery(document).on('click', 'a[data-toggle]', function (e) {
        var img = jQuery(this).attr('data-img'),
            text = jQuery(this).attr('data-text'),
            prodil_name =  jQuery('#ins_name').val(),
            prodil_pic = jQuery('#ins_foto').val(),
            modal = jQuery('#modal');

        modal.find('.full-img').attr('src', img);
        modal.find('.desc').text(text);
        modal.find('.prof img').attr('src', prodil_pic);
        modal.find('.prof p').text(prodil_name);

        return false;
    });

});