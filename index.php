<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sidorov
 */

get_header();

if ( is_home() && is_front_page() ) : 
 $all_options = get_option('true_options');

if ($all_options['isInstagram']):?>
    <section class="instagram">
        <div class="container">
            <div class="content">
            <?php if ($all_options['instagram_head']): ?>
                 <div class="instagram__head">
                        <?php echo $all_options['instagram_head']; ?>
                </div>
            <?php endif;?>
                <?php echo do_shortcode("[instagram-feed num=5 cols=6 showfollow=true custom=true sortby=random]");?>
            </div>
        </div>
    </section>
<?php endif;
    

      if ($all_options['isSlider']):?>

    <section class="slider">
        <div class="container">
            <div class="content">
                <div class="slider__head">
                    <?php echo $all_options['slider-text']?>
                </div>
            </div>
            </div>
            <div class="owl-carousel">

                <?php
                $args = array(
                    'post_type' => 'slider',
                    'posts_per_page'=>999
                );
                $query = new WP_Query($args);
                if ($query->have_posts()):?>
                    <?php  while ( $query->have_posts() ) : $query->the_post(); ?>
                        <?php
                        $cat = get_field('cat');
                        ?>

                        <div style="background: url(<?php the_field('image')?>) center center; background-size: cover; height: 200px ">
                        <div class="container">
                            <div class="content">
                                    <div class="content__info">
                                        <p>ИЗБРАННОЕ: <?php echo $cat->name?></p>
                                    </div>
                                    <div class="content__text">
                                        <p><?php the_field('text')?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php  endwhile; endif; ?>
            </div>
        <!-- </div> -->
    </section>

        <?php endif;
endif;
?>
    <section class="categories-list">
        <div class="container">
            <div class="content">
                <div class="row">
                    <p class="name">
                        Рубрики:
                    </p>
                    <?php
                    $args = array(
                        'show_option_all'    => 'Все',
                        'show_option_none'   => __('No categories'),
                        'orderby'            => 'name',
                        'order'              => 'ASC',
                        'show_last_update'   => 0,
                        'style'              => 'list',
                        'show_count'         => 0,
                        'hide_empty'         => 1,
                        'use_desc_for_title' => 1,
                        'child_of'           => 0,
                        'feed'               => '',
                        'feed_type'          => '',
                        'feed_image'         => '',
                        'exclude'            => '',
                        'exclude_tree'       => '',
                        'include'            => '',
                        'hierarchical'       => true,
                        'title_li'           => '',
                        'number'             => NULL,
                        'echo'               => 1,
                        'depth'              => 0,
                        'current_category'   => 0,
                        'pad_counts'         => 0,
                        'taxonomy'           => 'category',
                        'walker'             => 'Walker_Category',
                        'hide_title_if_empty' => false,
                        'separator'          => '<br />',
                        'echo' => false
                    );
                    ?>

                    <ul class="categories-list__items">
                        <?php
                            $categories = wp_list_categories($args);
                            if(strpos($categories,'current-cat') == false) {
                                $categories = str_replace('cat-item-all', 'cat-item-all current-cat', $categories);
                            }
                            echo $categories;
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="posts">
        <div class="container">
            <div class="content">
                <div class="content__posts">
                    <?php
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/content', get_post_format() );
                        endwhile;
                        ?>

                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-2">
                               <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); }?>
                            </div>
                        </div>
                        <?php
                    else:
                        get_template_part( 'template-parts/empty-content', '' );
                    endif; ?>
                </div>
            </div>
        </div>
    </section>






<?php
get_footer();
