

<div class="row post-container">
    <div class="col-xs-2 content__posts__info">
        <?php
            $categoriesList = wp_get_post_categories( $post->ID, array('fields' => 'all') );
          if ($categoriesList) :
              foreach( $categoriesList as $cat ):
        ?>
            <p class="post-category">
              <?php  echo $cat->name?>
            </p><br>
        <?php
              endforeach;
        endif;?>
        <p class="date">
            <?php echo the_time('j F Y')?>
        </p>
    </div>
    <div class="col-xs-9 ">
        <div class="post-preview">
            <h2>
                <a class="tittle" href="<?php the_permalink()?>">
                    <?php the_title() ?>
                </a>
            </h2>
            <?php
            $t = wp_get_post_tags($post->ID);
            if ($t):
            ?>
            <ul class="tags">
                <?php foreach ($t as $item):?>
                    <li><a href="<?php echo get_tag_link($item->term_id); ?>"><?php echo $item->name?></a></li>
                <?php endforeach;?>
            </ul>

            <?php endif;?>

            <div class="post-body">
                <?php the_content() ?>
                <a href="<?php the_permalink()?>" class="more">Дальше интересней</a>
            </div>

        </div>
    </div>
</div>