<?php
/**
 * sidorov functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sidorov
 */

if ( ! function_exists( 'sidorov_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function sidorov_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on sidorov, use a find and replace
	 * to change 'sidorov' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'sidorov', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'sidorov' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'sidorov_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'sidorov_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sidorov_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sidorov_content_width', 640 );
}
add_action( 'after_setup_theme', 'sidorov_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sidorov_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'sidorov' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'sidorov' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'sidorov_widgets_init' );


function my_styles() {
    wp_enqueue_style('default', get_template_directory_uri() . '/public/assets/css/main.min.css');
}

function owl_carousel() {
    wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/public/assets/css/owl-carousel.min.css');
    wp_enqueue_script('owl.carousel.script', get_template_directory_uri() . '/public/assets/js/owl.carousel.min.js');
    wp_enqueue_script('main', get_template_directory_uri() . '/public/assets/js/main.min.js');
}


function my_scripts() {
    wp_enqueue_script('jquery', get_template_directory_uri() . '/public/assets/js/jquery-2.2.4.min.js');
    wp_enqueue_script('modal', get_template_directory_uri() . '/public/assets/js/bootstrap.min.js');
}



add_action( 'wp_enqueue_scripts', 'my_styles' );
add_action( 'wp_enqueue_scripts', 'my_scripts' );
add_action( 'wp', 'wpse47305_check_home' );

function wpse47305_check_home() {
    if ( is_home() )
        add_action( 'wp_enqueue_scripts', 'owl_carousel' );
}



show_admin_bar(false);


/*
 * Функция, добавляющая страницу в пункт меню Настройки
 */
function true_options() {
    global $true_page;
    add_options_page( 'Параметры', 'Параметры', 'manage_options', $true_page, 'true_option_page');
}
add_action('admin_menu', 'true_options');

/**
 * Возвратная функция (Callback)
 */
function true_option_page(){
    global $true_page;
    ?><div class="wrap">
    <h2>Дополнительные параметры сайта</h2>
    <form method="post" enctype="multipart/form-data" action="options.php">
        <?php
        settings_fields('true_options'); // меняем под себя только здесь (название настроек)
        do_settings_sections($true_page);
        ?>
        <p class="submit">
            <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
        </p>
    </form>
    </div><?php
}

/*
 * Регистрируем настройки
 * Мои настройки будут храниться в базе под названием true_options (это также видно в предыдущей функции)
 */
function true_option_settings() {
    global $true_page;
    // Присваиваем функцию валидации ( true_validate_settings() ). Вы найдете её ниже
    register_setting( 'true_options', 'true_options', 'true_validate_settings' ); // true_options

    add_settings_section( 'header', ' Шапка', '', $true_page );
    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'header',
        'desc'      => 'Шапка:'
    );
    add_settings_field( 'my_textarea_field_3', 'Шапка:', 'true_option_display_settings', $true_page, 'header', $true_field_params );


    // Добавляем секцию
    add_settings_section( 'true_section_1', 'Настройки подвала', '', $true_page );

    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'textarea_1',
        'desc'      => 'Социалочки:'
    );
    add_settings_field( 'my_textarea_field', 'Социалочки:', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'textarea_2',
        'desc'      => 'Проекты:'
    );
    add_settings_field( 'my_textarea_field_2', 'Проекты:', 'true_option_display_settings', $true_page, 'true_section_1', $true_field_params );

    add_settings_section( 'true_section_2', ' Слайдер', '', $true_page );

    $true_field_params = array(
        'type'      => 'checkbox',
        'id'        => 'isSlider',
        'desc'      => 'Отображать Слайдер'
    );
    add_settings_field( 'my_checkbox_field', 'Отображать Слайдер', 'true_option_display_settings', $true_page, 'true_section_2', $true_field_params );

    $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'slider-text',
        'desc'      => 'Заголовок слайдера:'
    );
    add_settings_field( 'my_textarea_field_3', 'Заголовок слайдера:', 'true_option_display_settings', $true_page, 'true_section_2', $true_field_params );

    add_settings_section( 'instagram', 'Настройки instagram', '', $true_page );

        $true_field_params = array(
        'type'      => 'checkbox',
        'id'        => 'isInstagram',
        'desc'      => 'Отображать Instagram'
    );
    add_settings_field( 'instagram_checkbox_field', 'Отображать Instagram', 'true_option_display_settings', $true_page, 'instagram', $true_field_params );

        $true_field_params = array(
        'type'      => 'textarea',
        'id'        => 'instagram_head',
        'desc'      => 'Заголовок Instagram:'
    );
    add_settings_field( 'instagram_textarea_field', 'Заголовок Instagram:', 'true_option_display_settings', $true_page, 'instagram', $true_field_params );

}

add_action( 'admin_init', 'true_option_settings' );

/*
 * Функция отображения полей ввода
 * Здесь задаётся HTML и PHP, выводящий поля
 */
function true_option_display_settings($args) {
    extract( $args );
    $option_name = 'true_options';
    $o = get_option( $option_name );
    switch ( $type ) {
        case 'text':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<input class='regular-text' type='text' id='$id' name='" . $option_name . "[$id]' value='$o[$id]' />";
            echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
            break;
        case 'textarea':
            $o[$id] = esc_attr( stripslashes($o[$id]) );
            echo "<textarea class='code large-text' cols='50' rows='10' type='text' id='$id' name='" . $option_name . "[$id]'>$o[$id]</textarea>";
            echo ($desc != '') ? "<br /><span class='description'>$desc</span>" : "";
            break;
        case 'checkbox':
            $checked = ($o[$id] == 'on') ? " checked='checked'" :  '';
            echo "<label><input type='checkbox' id='$id' name='" . $option_name . "[$id]' $checked /> ";
            echo ($desc != '') ? $desc : "";
            echo "</label>";
            break;
        case 'select':
            echo "<select id='$id' name='" . $option_name . "[$id]'>";
            foreach($vals as $v=>$l){
                $selected = ($o[$id] == $v) ? "selected='selected'" : '';
                echo "<option value='$v' $selected>$l</option>";
            }
            echo ($desc != '') ? $desc : "";
            echo "</select>";
            break;
        case 'radio':
            echo "<fieldset>";
            foreach($vals as $v=>$l){
                $checked = ($o[$id] == $v) ? "checked='checked'" : '';
                echo "<label><input type='radio' name='" . $option_name . "[$id]' value='$v' $checked />$l</label><br />";
            }
            echo "</fieldset>";
            break;
    }
}

/*
 * Функция проверки правильности вводимых полей
 */
function true_validate_settings($input) {
    foreach($input as $k => $v) {
        $valid_input[$k] = trim($v);
    }
    return $valid_input;
}

add_action( 'init', 'create_post_project' );

function create_post_project() {
    register_post_type( 'slider',
        array(
            'labels' => array(
                'name' => __( 'Слайдер' ),
                'singular_name' => __( 'slider' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );

}

function remove_post_type_page_from_search() {
    global $wp_post_types;
    $wp_post_types['slider']->exclude_from_search = true;
}
add_action('init', 'remove_post_type_page_from_search');