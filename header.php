<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sidorov
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- <div class="wrapper">
    <div class="wrapper__content"> -->

    <header>
        <div class="container">
            <a href="<?php echo  home_url()?>" class="logo">
                <img src="<?php echo get_template_directory_uri()?>/public/assets/images/content/logo.jpg" alt="">
            </a>
            <div class="content">
                <div class="row">
                    <div class="col-xs-8 content__about">
                        <?php $all_options = get_option('true_options');
                        if ($all_options['header']) {
                            echo $all_options['header'];
                        }
                        ?>
                    </div>
                    <div class="col-xs-4">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
