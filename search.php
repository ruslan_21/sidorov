<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package sidorov
 */

get_header(); ?>
    <div  class="container">
        <div class="content">
            <a href="<?php echo  home_url()?>" class="go-home">На главную</a>
        </div>
    </div>
    <section class="categories-list">
        <div class="container">
            <div class="content">
                <div class="row">
                    <p class="name">
                        Рубрики:
                    </p>
                    <?php
                    $args = array(
                        'show_option_all'    => 'Все',
                        'show_option_none'   => __('No categories'),
                        'orderby'            => 'name',
                        'order'              => 'ASC',
                        'show_last_update'   => 0,
                        'style'              => 'list',
                        'show_count'         => 0,
                        'hide_empty'         => 1,
                        'use_desc_for_title' => 1,
                        'child_of'           => 0,
                        'feed'               => '',
                        'feed_type'          => '',
                        'feed_image'         => '',
                        'exclude'            => '',
                        'exclude_tree'       => '',
                        'include'            => '',
                        'hierarchical'       => true,
                        'title_li'           => '',
                        'number'             => NULL,
                        'echo'               => 1,
                        'depth'              => 0,
                        'current_category'   => 0,
                        'pad_counts'         => 0,
                        'taxonomy'           => 'category',
                        'walker'             => 'Walker_Category',
                        'hide_title_if_empty' => false,
                        'separator'          => '<br />',
                    )
                    ?>

                    <ul class="categories-list__items">
                        <?php wp_list_categories( $args );?>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="posts">
        <div class="container">
            <div class="content">
                <div class="content__posts">
                    <?php
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/content', get_post_format() );
                        endwhile;
                        ?>

                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-2">
                                <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); }?>
                            </div>
                        </div>
                    <?php
                    else:
                        get_template_part( 'template-parts/empty-content', '' );
                    endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
