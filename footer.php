<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sidorov
 */
$all_options = get_option('true_options');
?>
<!-- </div> -->
<footer>
    <div class="container">
        <div class="content">
            <p>© 1996–2017 Роман Сидоров</p>
            <p class="footer__links">Социалочки:
                <?php echo $all_options['textarea_1']; ?>
            </p>
            <p class="footer__links">Проекты:
                <?php echo $all_options['textarea_2']; ?>
            </p>
        </div>
    </div>
</footer>
<!-- </div> -->

<div class="modal fade" id='modal' tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
            <div class="row">
                <div class="col-xs-8">
                    <img src="" class="full-img">
                </div>
                <div class="col-xs-4">
                    <div class="prof">
                        <img src="">
                        <p></p>
                    </div>
                    <p class="desc"></p>
                </div>
            </div>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<input type="hidden" id="ins_name">
<input type="hidden" id="ins_foto">
<?php wp_footer(); ?>

</body>
</html>
