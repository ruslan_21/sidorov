<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sidorov
 */

get_header(); ?>

    <div  class="container">
        <div class="content">
            <a href="<?php echo  home_url()?>" class="go-home">На главную</a>
        </div>
    </div>

    <section class="post">
        <div class="container">
            <div class="content">
                <div class="content__post">
                    <?php
                    if ( have_posts() ) :
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/single-content', get_post_format() );
                        endwhile;
                    else :
                        get_template_part( 'template-parts/empty-content', '' );
                    endif; ?>
                </div>
            </div>
        </div>
    </section>


<?php
get_footer();
