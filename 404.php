<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sidorov
 */

get_header(); ?>
    <div class="container text-center">

        <div class="clearfix">
            <div class="fullwidth">
                <div class="error-page">
                    <h1>404</h1>
                    <h2>Страница не найдена</h2>
                    <p>Запрашиваемая страница не может быть найдена. Это может быть орфографическая ошибка в URL-адресе или удаленной странице.</p>
                    <a class="feat-more button-dark" href="<?php echo  home_url()?>">Назад на главную</a>
                </div><!-- end .error-page -->
            </div><!-- end #main -->
        </div><!-- end #content -->
    </div>

<?php
get_footer();
